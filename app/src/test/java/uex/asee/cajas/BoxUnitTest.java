package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Models.Box;

import org.junit.Test;

public class BoxUnitTest {

    @Test
    public void shouldGetIdOfBox () {
        Box aux = new Box();
        aux.set_id("1");
        assertEquals("1",aux.get_id());
    }

    @Test
    public void shouldGetNameOfBox () {
        Box aux = new Box();
        aux.setName("box");
        assertEquals("box",aux.getName());
    }

    @Test
    public void shouldGetIdCasaOfBox () {
        Box aux = new Box();
        aux.setIdcasa("1");
        assertEquals("1",aux.getIdcasa());
    }

    @Test
    public void shouldGetImgInfoOfBox () {
        Box aux = new Box();
        aux.setImginfo("img");
        assertEquals("img",aux.getImginfo());
    }

    @Test
    public void shouldGetDescriptionOfBox () {
        Box aux = new Box();
        aux.setDescription("desc");
        assertEquals("desc",aux.getDescription());
    }

    @Test
    public void shouldGetUserIdOfBox () {
        Box aux = new Box();
        aux.setUserid("1");
        assertEquals("1",aux.getUserid());
    }
}
