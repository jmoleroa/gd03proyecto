package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Entidades.Caja;

import org.junit.Test;

public class CajaUnitTest {

    @Test
    public void shouldGetIdOfCaja () {
        Caja aux = new Caja("1","","","","","");
        assertEquals("1",aux.getId());
    }

    @Test
    public void shouldSetIdOfCaja () {
        Caja aux = new Caja("1","","","","","");
        aux.setId("2");
        assertEquals("2",aux.getId());
    }

    @Test
    public void shouldGetNameOfCaja () {
        Caja aux = new Caja("1","caja","","","","");
        assertEquals("caja",aux.getName());
    }

    @Test
    public void shouldSetNameOfCaja () {
        Caja aux = new Caja("1","caja","","","","");
        aux.setName("otraCaja");
        assertEquals("otraCaja",aux.getName());
    }

    @Test
    public void shouldGetIdCasaOfCaja () {
        Caja aux = new Caja("1","caja","1","","","");
        assertEquals("1",aux.getIdCasa());
    }

    @Test
    public void shouldSetIdCasaOfCaja () {
        Caja aux = new Caja("1","caja","1","","","");
        aux.setIdCasa("2");
        assertEquals("2",aux.getIdCasa());
    }

    @Test
    public void shouldGetDescriptionOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","","");
        assertEquals("desc",aux.getDescription());
    }

    @Test
    public void shouldSetDescriptionOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","","");
        aux.setDescription("description");
        assertEquals("description",aux.getDescription());
    }

    @Test
    public void shouldGetUserIdOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","1","");
        assertEquals("1",aux.getUserId());
    }

    @Test
    public void shouldSetUserIdOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","1","");
        aux.setUserId("2");
        assertEquals("2",aux.getUserId());
    }

    @Test
    public void shouldGetImagenOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","1","img");
        assertEquals("img",aux.getImagen());
    }

    @Test
    public void shouldSetImagenOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","1","img");
        aux.setImagen("imagen");
        assertEquals("imagen",aux.getImagen());
    }

    @Test
    public void shouldToStringOfCaja () {
        Caja aux = new Caja("1","caja","1","desc","1","img");
        String toString = aux.toString();
        assertEquals("1"+System.getProperty("line.separator")+"caja"+System.getProperty("line.separator")+"1"+
                System.getProperty("line.separator")+"desc"+System.getProperty("line.separator")+"1",toString);
    }
}
