package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Entidades.Casa;

import org.junit.Test;

public class CasaUnitTest {

    @Test
    public void shouldGetIdOfCasa() {
        Casa casa = new Casa("1","","");
        assertEquals("1",casa.getId());
    }

    @Test
    public void shouldSetIdOfCasa() {
        Casa casa = new Casa("1","","");
        casa.setId("2");
        assertEquals("2",casa.getId());
    }

    @Test
    public void shouldGetNameOfCasa() {
        Casa casa = new Casa("1","casa","");
        assertEquals("casa",casa.getName());
    }

    @Test
    public void shouldSetNameOfCasa() {
        Casa casa = new Casa("1","casa","");
        casa.setName("otra");
        assertEquals("otra",casa.getName());
    }

    @Test
    public void shouldGetDescriptionOfCasa() {
        Casa casa = new Casa("1","casa","desc");
        assertEquals("desc",casa.getDescription());
    }

    @Test
    public void shouldSetDescriptionOfCasa() {
        Casa casa = new Casa("1","casa","desc");
        casa.setDescription("description");
        assertEquals("description",casa.getDescription());
    }

    @Test
    public void shouldToStringOfCasa() {
        Casa casa = new Casa("1","casa","desc");
        String toString = casa.toString();
        assertEquals("1"+System.getProperty("line.separator")+"casa"+System.getProperty("line.separator")+"desc",toString);
    }
}
