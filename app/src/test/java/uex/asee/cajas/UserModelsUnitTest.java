package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Models.User;

import org.junit.Test;

public class UserModelsUnitTest {

    @Test
    public void shouldGetUsernameOfUser() {
        User user = new User();
        user.setUsername("user");
        assertEquals("user",user.getUsername());
    }

    @Test
    public void shouldGetEmailOfUser() {
        User user = new User();
        user.setEmail("email");
        assertEquals("email",user.getEmail());
    }
}
