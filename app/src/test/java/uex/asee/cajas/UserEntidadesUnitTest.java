package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Entidades.User;

import org.junit.Test;

public class UserEntidadesUnitTest {

    @Test
    public void shouldGetIdOfUser() {
        User user = new User(1,"","","");
        assertEquals(1,user.getId());
    }

    @Test
    public void shouldSetIdOfUser() {
        User user = new User(1,"","","");
        user.setId(2);
        assertEquals(2,user.getId());
    }

    @Test
    public void shouldGetUsernameOfUser() {
        User user = new User(1,"user","","");
        assertEquals("user",user.getUsername());
    }

    @Test
    public void shouldSetUsernameOfUser() {
        User user = new User(1,"user","","");
        user.setUsername("username");
        assertEquals("username",user.getUsername());
    }

    @Test
    public void shouldGetPasswordOfUser() {
        User user = new User(1,"user","pass","");
        assertEquals("pass",user.getPassword());
    }

    @Test
    public void shouldSetPasswordOfUser() {
        User user = new User(1,"user","pass","");
        user.setPassword("password");
        assertEquals("password",user.getPassword());
    }

    @Test
    public void shouldGetEmailOfUser() {
        User user = new User(1,"user","pass","email");
        assertEquals("email",user.getEmail());
    }

    @Test
    public void shouldSetEmailOfUser() {
        User user = new User(1,"user","pass","email");
        user.setEmail("user@gmail.com");
        assertEquals("user@gmail.com",user.getEmail());
    }

    @Test
    public void shouldToStringOfUser() {
        User user = new User(1,"user","pass","email");
        String toString = user.toString();
        assertEquals("1"+System.getProperty("line.separator")+"user"+System.getProperty("line.separator")+"pass"+System.getProperty("line.separator")+"email",toString);
    }
}
