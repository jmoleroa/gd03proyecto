package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Entidades.Objeto;

import org.junit.Test;

public class ObjetoUnitTest {

    @Test
    public void shouldGetIdOfObjeto() {
        Objeto obj = new Objeto("1","","","","","","");
        assertEquals("1",obj.getId());
    }

    @Test
    public void shouldSetIdOfObjeto() {
        Objeto obj = new Objeto("1","","","","","","");
        obj.setId("2");
        assertEquals("2",obj.getId());
    }

    @Test
    public void shouldGetNameOfObjeto() {
        Objeto obj = new Objeto("1","obj","","","","","");
        assertEquals("obj",obj.getName());
    }

    @Test
    public void shouldSetNameOfObjeto() {
        Objeto obj = new Objeto("1","obj","","","","","");
        obj.setName("objeto");
        assertEquals("objeto",obj.getName());
    }

    @Test
    public void shouldGetCategoriaOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","","","","");
        assertEquals("categ",obj.getCategoria());
    }

    @Test
    public void shouldSetCategoriaOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","","","","");
        obj.setCategoria("categoria");
        assertEquals("categoria",obj.getCategoria());
    }

    @Test
    public void shouldGetImagenOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","","","");
        assertEquals("img",obj.getImagen());
    }

    @Test
    public void shouldSetImagenOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","","","");
        obj.setImagen("imagen");
        assertEquals("imagen",obj.getImagen());
    }

    @Test
    public void shouldGetDescriptionOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","","");
        assertEquals("desc",obj.getDescription());
    }

    @Test
    public void shouldSetDescriptionOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","","");
        obj.setDescription("description");
        assertEquals("description",obj.getDescription());
    }

    @Test
    public void shouldGetCajaOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","1","");
        assertEquals("1",obj.getCaja());
    }

    @Test
    public void shouldSetCajaOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","1","");
        obj.setCaja("2");
        assertEquals("2",obj.getCaja());
    }

    @Test
    public void shouldGetUserIdOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","1","1");
        assertEquals("1",obj.getUserId());
    }

    @Test
    public void shouldSetUserIdOfObjeto() {
        Objeto obj = new Objeto("1","obj","categ","img","desc","1","1");
        obj.setUserId("2");
        assertEquals("2",obj.getUserId());
    }

    @Test
    public void shouldToStringOfObjeto () {
        Objeto obj = new Objeto("1","obj","categ","img","desc","1","1");
        String toString = obj.toString();
        assertEquals("1"+System.getProperty("line.separator")+"obj"+System.getProperty("line.separator")+"categ"+
                System.getProperty("line.separator")+"desc"+System.getProperty("line.separator")+"1"+System.getProperty("line.separator")+
                "1"+System.getProperty("line.separator")+"img",toString);
    }
}
