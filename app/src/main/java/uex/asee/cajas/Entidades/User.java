package uex.asee.cajas.Entidades;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public static final String ID = "ID";

    @Ignore
    public static final String USERNAME = "USERNAME";

    @Ignore
    public static final String PASSWORD = "PASSWORD";

    @Ignore
    public static final String EMAIL = "EMAIL";

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "username")
    private String username = new String();

    @ColumnInfo(name = "password")
    private String password = new String();

    @ColumnInfo(name = "email")
    private String email = new String();

    @Ignore
    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }


    public User(long id, String username, String password, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    @Ignore
    User(Intent intent) {
        id = intent.getLongExtra(User.ID, 0);
        username = intent.getStringExtra(User.USERNAME);
        password = intent.getStringExtra(User.PASSWORD);
        email = intent.getStringExtra(User.EMAIL);
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public static void packageIntent (Intent intent, String username, String password, String email) {
        intent.putExtra(User.USERNAME, username);
        intent.putExtra(User.PASSWORD, password);
        intent.putExtra(User.EMAIL, email);
    }

    public String toString() {
        return  id + ITEM_SEP + username + ITEM_SEP + password + ITEM_SEP + email;
    }

    public String toLog() {
        return "ID: " + id + ITEM_SEP + " Nombre de usuario: " + username + ITEM_SEP +
                " Contraseña: " + password + ITEM_SEP + " Email: " + email;
    }
}
