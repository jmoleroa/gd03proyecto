package uex.asee.cajas.Adapters;

import java.util.List;

import uex.asee.cajas.Entidades.Caja;

public interface OnBoxesLoadedListener {
    public void onBoxesLoaded(List<Caja> boxes);
}
