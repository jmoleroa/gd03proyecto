package uex.asee.cajas.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ImageAPI;
import uex.asee.cajas.R;
import uex.asee.cajas.remote.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.Base64;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemAdapter extends BaseAdapter {
    private Context context;
    private List<Objeto> items;
    private OnClickListener ocl;
    LayoutInflater inflater;

    public ItemAdapter(Context c, List<Objeto> items, OnClickListener ol) {
        this.context = c;
        this.items = items;
        this.ocl = ol;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public interface OnClickListener{
        void OnClick(Objeto objeto, View view);
    }
    public int getCount(){
        if(this.items!=null) {
            System.out.println("ITEMS  NO NULOS----------------------------------------------------------------------------------");
            return this.items.size();
        }
        else {
            System.out.println("ITEMS NULOS----------------------------------------------------------------------------------");
            return 0;
        }
    }
    public Objeto getItem(int pos){
        return this.items.get(pos);
    }
    public long getItemId (int pos){
        return 0;
    }
    public View getView(int position, View view, ViewGroup viewGroup){
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }

        final Objeto item = getItem(position);
        TextView nombreItem = (TextView) view.findViewById(R.id.nombre_item);
        ImageView imageView = (ImageView) view.findViewById(R.id.imagen_item);
        nombreItem.setText(item.getName());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ocl.OnClick(item,view);
            }
        });

        ImageAPI imageAPI = RetrofitClient.getClient().create(ImageAPI.class);
        Call<String> call = imageAPI.downloadFileWithDynamicUrlSync(item.getImagen());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    byte [] decodedString = Base64.getDecoder().decode(response.body());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } else {
                    Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
            }
        });
        return view;
    }
    public void swap (List<Objeto> lista_objeto){
        items = lista_objeto;
    }
}
