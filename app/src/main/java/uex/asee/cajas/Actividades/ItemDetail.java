package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ImageAPI;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetail extends AppCompatActivity {
    public static final String ITEM_ID = "ITEM_ID";
    String item_id;
    private Objeto itemDetallado;
    private TextView textExtendida;
    private TextView itemDesc;
    private TextView itemCat;
    private TextView itemBox;
    ImageView imageView;
    public static List<Objeto> objetos = new ArrayList<Objeto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        textExtendida = (TextView) findViewById(R.id.item_name);
        itemDesc = (TextView) findViewById(R.id.item_desc);
        itemCat = (TextView) findViewById(R.id.item_cate);
        itemBox = (TextView) findViewById(R.id.item_box);
        item_id = getIntent().getStringExtra(ITEM_ID);
        imageView = findViewById(R.id.image_detail_item);

    }
    @Override
    protected void onStart() {
        super.onStart();
        cargarDatos();

    }

    private void showDetails(){
        textExtendida.setText(itemDetallado.getName());
        itemDesc.setText(itemDetallado.getDescription());
        itemCat.setText(itemDetallado.getCategoria());
        ImageAPI imageAPI = RetrofitClient.getClient().create(ImageAPI.class);
        Call<String> call = imageAPI.downloadFileWithDynamicUrlSync(itemDetallado.getImagen());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    byte [] decodedString = Base64.getDecoder().decode(response.body());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } else {
                    Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
            }
        });
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                InventarioDatabase database = InventarioDatabase.getInstance(ItemDetail.this);
                Caja box = database.getDaoCaja().getCajaById(itemDetallado.getCaja());
                if(box != null){
                    itemBox.setText(box.getName());
                }
                Button editar = (Button) findViewById(R.id.button_edit_item);
                editar.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        String str = getIntent().getStringExtra(ITEM_ID);
                        Intent intent = new Intent(getApplicationContext(), EditItem.class);
                        intent.putExtra(EditItem.EDIT_ITEM_ID, str);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void cargarDatos(){
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        Call<Item> call = itemAPI.getItem(Login.TOKEN.getToken(),item_id);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if(response.isSuccessful()){
                    Item item = response.body();
                    itemDetallado = new Objeto(item.get_id(),item.getName(),item.getCategoria(), item.getImginfo(), item.getDescription(),item.getIdcaja(),item.getUserid());
                    runOnUiThread(() -> showDetails());
                }else{
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(ItemDetail.this);
                        itemDetallado = database.getDaoObjeto().getObjetoById(item_id);
                        runOnUiThread(() -> showDetails());
                    });
                }
            }
            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    InventarioDatabase database = InventarioDatabase.getInstance(ItemDetail.this);
                    itemDetallado = database.getDaoObjeto().getObjetoById(item_id);
                    runOnUiThread(() -> showDetails());
                });
            }
        });
    }


}