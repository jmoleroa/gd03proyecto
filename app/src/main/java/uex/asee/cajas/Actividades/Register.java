package uex.asee.cajas.Actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import uex.asee.cajas.Interfaces.UserAPI;
import uex.asee.cajas.Models.User;
import uex.asee.cajas.R;
import uex.asee.cajas.remote.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText emailText = (EditText) findViewById(R.id.emailreg);
        final EditText passwordText = (EditText) findViewById(R.id.passwordreg);
        final EditText password2Text = (EditText) findViewById(R.id.passwordreg2);
        final EditText usernameText = (EditText) findViewById(R.id.usernamereg);

        Button button = (Button) findViewById(R.id.registerButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!emailText.getText().toString().isEmpty() && !passwordText.getText().toString().isEmpty() && !password2Text.getText().toString().isEmpty()
                        && !usernameText.getText().toString().isEmpty()){
                    if(passwordText.getText().toString().contains(password2Text.getText().toString())){
                        RegisterUser(emailText.getText().toString(),passwordText.getText().toString(),usernameText.getText().toString());
                    }else{
                        Toast.makeText(Register.this, "Las constraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(Register.this, "Has dejado campos sin rellenar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void RegisterUser(String email,String password,String username){
        UserAPI userAPI = RetrofitClient.getClient().create(UserAPI.class);
        Call<User> call = userAPI.registerUser(email,username,password);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()){
                    User user = response.body();
                    System.out.println(user.getUsername());
                    System.out.println(user.getEmail());
                    Intent intent = new Intent(getBaseContext(), Login.class);
                    startActivity(intent);
                }else{
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(Register.this, jObjError.getString("msg"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(Register.this, "Error interno", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(Register.this, "Error interno", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(Register.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
