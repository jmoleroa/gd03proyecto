package uex.asee.cajas.Actividades;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import uex.asee.cajas.Adapters.ItemAdapter;
import uex.asee.cajas.AppContainer;
import uex.asee.cajas.Interfaces.ImageAPI;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.HousesViewModel;
import uex.asee.cajas.ui.ItemsViewModel;

public class EditCaja extends AppCompatActivity {
    public static final String EDIT_CAJA_ID = "EDIT_CAJA_ID";
    private Caja cajaDetallada;
    private TextView textExtendida;
    ArrayList<Casa> houses = new ArrayList<>();
    String caja_id;
    Spinner spinner;
    String idCasa;
    ArrayList<String> casasNames= new ArrayList<>();
    ArrayList<Casa> casas= new ArrayList<>();
    EditText nBoxName;
    EditText nBoxDesc;
    ImageView imageView;
    Uri auxuri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_caja);
        caja_id = getIntent().getStringExtra(EDIT_CAJA_ID);
        nBoxName = findViewById(R.id.ecaja_name);
        nBoxDesc = findViewById(R.id.nbox_desc);
        spinner = findViewById(R.id.spinner);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        imageView = findViewById(R.id.image_selected_edit_caja);
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    System.out.println(uri);
                    imageView.setImageURI(uri);
                    auxuri = uri;
                });
        Button btnChooseFile = findViewById(R.id.select_photo_box_edit);
        btnChooseFile.setOnClickListener(view -> {
            mGetContent.launch("image/*");
        });
        cargarDatos();


        AppContainer appContainer = MainActivity.appContainer;

        HousesViewModel mViewModel = new ViewModelProvider(this, appContainer.housesfactory).get(HousesViewModel.class);

        mViewModel.getCasas().observe(this, casas -> {

            if (casas != null) {
                houses = (ArrayList<Casa>) casas;
                System.out.println("TAMA´ÑO CASAS " + casas.size());
                for (Casa house : casas) {
                    casasNames.add(house.getName());

                }
                System.out.println("TAMAÑO CASASNOM " + casasNames.size());
            }
            ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, casasNames);
            spinner.setAdapter(adapter2);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (spinner.getSelectedItemPosition() > -1){
                        idCasa = houses.get(spinner.getSelectedItemPosition()).getId();
                        System.out.println("HOLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });


    }
    private void cargarDatos(){
        BoxAPI boxAPI = RetrofitClient.getClient().create(BoxAPI.class);
        Call<Box> call = boxAPI.getBox(Login.TOKEN.getToken(),caja_id);

        call.enqueue(new Callback<Box>() {
            @Override
            public void onResponse(Call<Box> call, Response<Box> response) {
                if(response.isSuccessful()){
                    Box caja = response.body();
                    cajaDetallada = new Caja(caja.get_id(),caja.getName(),caja.getIdcasa(), caja.getDescription(), caja.getUserid(),caja.getImginfo());
                    runOnUiThread(() -> showDetails());
                }else{
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        InventarioDatabase database = InventarioDatabase.getInstance(EditCaja.this);
                        cajaDetallada = database.getDaoCaja().getCajaById(caja_id);
                        runOnUiThread(() -> showDetails());
                    });
                }
            }
            @Override
            public void onFailure(Call<Box> call, Throwable t) {
                AppExecutors.getInstance().diskIO().execute(() -> {
                    InventarioDatabase database = InventarioDatabase.getInstance(EditCaja.this);
                    cajaDetallada = database.getDaoCaja().getCajaById(caja_id);
                    runOnUiThread(() -> showDetails());
                });
            }
        });
    }
    private void showDetails(){
        nBoxName.setText(cajaDetallada.getName());
        nBoxDesc.setText(cajaDetallada.getDescription());
        ImageAPI imageAPI = RetrofitClient.getClient().create(ImageAPI.class);
        Call<String> call = imageAPI.downloadFileWithDynamicUrlSync(cajaDetallada.getImagen());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    byte [] decodedString = Base64.getDecoder().decode(response.body());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } else {
                    Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Picasso.get().load("https://res.cloudinary.com/teepublic/image/private/s--1GXM2y6u--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_auto,h_630,q_90,w_630/v1587938872/production/designs/9519223_0.jpg").into(imageView);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final EditText nCajaName = (EditText) findViewById(R.id.ecaja_name);
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                System.out.println("valor spinner " + spinner.getSelectedItem() + " posicion " +spinner.getSelectedItemPosition());
                System.out.println(" id casa " + idCasa);
                cajaDetallada.setName(nBoxName.getText().toString());
                cajaDetallada.setDescription(nBoxDesc.getText().toString());
                cajaDetallada.setIdCasa(idCasa);
                if(!nBoxName.getText().toString().isEmpty() && !nBoxDesc.getText().toString().isEmpty()){
                    updateBox(nBoxName.getText().toString(),nBoxDesc.getText().toString(),idCasa);
                    this.finish();
                    return true;
                }else{
                    Toast.makeText(EditCaja.this, "Por favor rellena todos los campos", Toast.LENGTH_SHORT).show();
                }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateBox(String name,String description,String idcasa){
        BoxAPI houseAPI = RetrofitClient.getClient().create(BoxAPI.class);
        RequestBody rname = RequestBody.create(MediaType.parse("text/plain"),name);
        RequestBody rdescription = RequestBody.create(MediaType.parse("text/plain"),description);
        RequestBody ridcasa = null;
        if(idcasa != null){
            ridcasa = RequestBody.create(MediaType.parse("text/plain"),idcasa);
        }
        RequestBody rimg = null;
        MultipartBody.Part rbimg = null;
        try {
            if(auxuri !=  null){
                InputStream is = getContentResolver().openInputStream(auxuri);
                if(is!= null){
                    rimg = RequestBody.create(
                            MediaType.parse(getContentResolver().getType(auxuri)),
                            getBytes(is)

                    );
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String type = mime.getExtensionFromMimeType(getContentResolver().getType(auxuri));

                    rbimg =  MultipartBody.Part.createFormData("image", name+"."+type, rimg);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Call<Box> call = houseAPI.updateBox(Login.TOKEN.getToken(),caja_id,rname,ridcasa,rbimg,rdescription);

        call.enqueue(new Callback<Box>() {
            @Override
            public void onResponse(Call<Box> call, Response<Box> response) {
                if(response.isSuccessful()){
                    String id = response.body().get_id();
                    String name =  response.body().getName();
                    String idCasa =  response.body().getIdcasa();
                    String description = response.body().getDescription();
                    String userId = response.body().getUserid();
                    String imagen = response.body().getImginfo();
                    if(imagen == null){
                        imagen = "";
                    }
                    if(idCasa == null){
                        idCasa = "";
                    }
                    if(description == null){
                        description = "";
                    }
                    Caja caja = new Caja(id,name,idCasa,description,userId,imagen);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            InventarioDatabase database = InventarioDatabase.getInstance(EditCaja.this);
                            database.getDaoCaja().update(caja);
                        }
                    });
                }else{
                    Toast.makeText(EditCaja.this, "Error al editar caja", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Box> call, Throwable t) {
                Toast.makeText(EditCaja.this, "Error al editar caja", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }
}