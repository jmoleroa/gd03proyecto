package uex.asee.cajas.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.remote.Objetos.ItemsRepository;


public class ItemsViewModel extends ViewModel {
    private final ItemsRepository mItemsRepository; //1-Depende del repositorio
    private final LiveData<List<Objeto>> mItems; //2-Guarda un livedata de una lista de repositorios (la funcion de la app es mostrar la lista)
    //4-Se inyecta la dependencia del repositorio en el constructor
    public ItemsViewModel(ItemsRepository repository) {
        mItemsRepository = repository;
        mItems = mItemsRepository.getCurrentItems();


    }


    public void onRefresh() {  mItemsRepository.onRefresh(); }
    public LiveData<List<Objeto>> getItems() {
        return mItems;
    }
    public LiveData<List<Objeto>> getItemsById(String id) {

        MutableLiveData<List<Objeto>> rboxes = new MutableLiveData<>();
        List<Objeto> aux = new ArrayList<Objeto>();
        List<Objeto>aux1 = mItems.getValue();
        if (mItems.getValue() != null){
            for (Objeto c : aux1) {
                System.out.println(id);
                if (c.getCaja() != null) {
                    if (c.getCaja().equals(id)) {
                        aux.add(c);
                        System.out.println("Entra" + aux.size());
                    }
                }
            }
            rboxes.postValue(aux);
        }
        return rboxes;
    }
}
