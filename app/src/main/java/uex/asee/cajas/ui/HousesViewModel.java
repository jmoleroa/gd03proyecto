package uex.asee.cajas.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.remote.Casas.HousesRepository;

public class HousesViewModel extends ViewModel {

    private final HousesRepository mHousesRepository; //1-Depende del repositorio
    private final LiveData<List<Casa>> mCasas;


    public HousesViewModel(HousesRepository repository) {
        mHousesRepository = repository;
        mCasas = mHousesRepository.getCurrentHouses();


    }



    public LiveData<List<Casa>> getCasas() {
        return mCasas;
    }
}
