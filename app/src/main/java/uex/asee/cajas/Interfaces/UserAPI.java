package uex.asee.cajas.Interfaces;

import uex.asee.cajas.Models.Token;
import uex.asee.cajas.Models.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserAPI {
    @POST("user/register")
    @FormUrlEncoded
    public Call<User> registerUser(@Field("email") String email,
                                   @Field("username") String username,
                                   @Field("password") String password);

    @POST("user/login")
    @FormUrlEncoded
    public Call<Token> loginUser(@Field("email") String email,
                                 @Field("password") String password);

    @POST("user/logout")
    public Call<String> logoutUser(@Header("auth-token") String authtoken);

    @POST("user/deleteall")
    public Call<ResponseBody> deleteAll(@Header("auth-token") String authtoken);
}
