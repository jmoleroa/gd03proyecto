package uex.asee.cajas.Roomdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import uex.asee.cajas.Entidades.User;

import java.util.List;

@Dao
public interface UserDAO {
    @Query("SELECT * FROM user")
    public List<User> getAll();

    @Insert
    public long insert(User user);

    @Query("DELETE FROM user")
    public void deleteAll();

    @Query("DELETE FROM user WHERE ID = :id")
    public void deleteUserById(long id);

    @Update
    public int update(User user);
}
