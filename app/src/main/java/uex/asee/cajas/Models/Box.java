package uex.asee.cajas.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Box {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("idcasa")
    @Expose
    private String idcasa;
    @SerializedName("imginfo")
    @Expose
    private String imginfo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("userid")
    @Expose
    private String userid;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcasa() {
        return idcasa;
    }

    public void setIdcasa(String idcasa) {
        this.idcasa = idcasa;
    }

    public String getImginfo() {
        return imginfo;
    }

    public void setImginfo(String imginfo) {
        this.imginfo = imginfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
