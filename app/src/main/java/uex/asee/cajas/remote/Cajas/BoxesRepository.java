package uex.asee.cajas.remote.Cajas;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Roomdb.CajaDAO;
import uex.asee.cajas.remote.AppExecutors;

public class BoxesRepository {
    private static final String LOG_TAG = BoxesRepository.class.getSimpleName();

    // For Singleton instantiation
    private static BoxesRepository sInstance;
    private static AppExecutors appExecutors = AppExecutors.getInstance();
    private final CajaDAO mCajaDAO;
    private final BoxesNetworkDataSource mBoxesNetworkDataSource;

    private BoxesRepository(CajaDAO cajaDAO, BoxesNetworkDataSource boxesNetworkDataSource) {
        mCajaDAO = cajaDAO;
        mBoxesNetworkDataSource = boxesNetworkDataSource;
        // LiveData that fetches repos from network
        mBoxesNetworkDataSource.fetchCajas();
        LiveData<List<Caja>> networkData = mBoxesNetworkDataSource.getCurrentCajas();

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newCajasFromNetwork -> {
            appExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newCajasFromNetwork == null || newCajasFromNetwork.size() == 0){
                    mBoxesNetworkDataSource.setFromDatabase(cajaDAO.getAll().getValue());

                }
                else {
                    mCajaDAO.bulkInsert(newCajasFromNetwork);
                    Log.d(LOG_TAG, "New values inserted in Room");
                }

            });
        });
    }

    public synchronized static BoxesRepository getInstance(CajaDAO dao, BoxesNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new BoxesRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }
    public void onRefresh (){
        mBoxesNetworkDataSource.fetchCajas();
        System.out.println("Actualizando cajas "+mBoxesNetworkDataSource.getCurrentCajas().getValue().size());
    }

    /**
     * Database related operations
     **/

    public LiveData<List<Caja>> getCurrentBoxes() {
        //  Return LiveData from Room. Use Transformation to get owner
        return mBoxesNetworkDataSource.getCurrentCajas();
    }

}
