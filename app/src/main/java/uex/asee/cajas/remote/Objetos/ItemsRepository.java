package uex.asee.cajas.remote.Objetos;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;


import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Roomdb.ObjetoDAO;
import uex.asee.cajas.remote.AppExecutors;


public class ItemsRepository {
    private static final String LOG_TAG = ItemsRepository.class.getSimpleName();

    // For Singleton instantiation
    private static ItemsRepository sInstance;
    private static AppExecutors appExecutors = AppExecutors.getInstance();
    private final ObjetoDAO mObjetoDAO;
    private final ItemsNetworkDataSource mItemsNetworkDataSource;

    private ItemsRepository(ObjetoDAO objetoDAO, ItemsNetworkDataSource itemsNetworkDataSource) {
        mObjetoDAO = objetoDAO;
        mItemsNetworkDataSource = itemsNetworkDataSource;
        // LiveData that fetches repos from network
        mItemsNetworkDataSource.fetchItems();
        LiveData<List<Objeto>> networkData = mItemsNetworkDataSource.getCurrentItems();

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newItemsFromNetwork -> {
            appExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newItemsFromNetwork == null || newItemsFromNetwork.size() == 0){
                    mItemsNetworkDataSource.setFromDatabase(objetoDAO.getAll().getValue());

                }
                else {
                    mObjetoDAO.bulkInsert(newItemsFromNetwork);
                    Log.d(LOG_TAG, "New values inserted in Room");
                }

            });
        });
    }

    public synchronized static ItemsRepository getInstance(ObjetoDAO dao, ItemsNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new ItemsRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void onRefresh (){
        mItemsNetworkDataSource.fetchItems();
        System.out.println("Actualizando items "+mItemsNetworkDataSource.getCurrentItems().getValue().size());
    }

    /**
     * Database related operations
     **/

    public LiveData<List<Objeto>> getCurrentItems() {
        //  Return LiveData from Room. Use Transformation to get owner
        return mItemsNetworkDataSource.getCurrentItems();
    }
}
